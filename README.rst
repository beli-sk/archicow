ArchiCOW
========

Backup system supporting copy-on-write storage.

Development status: **Alpha**

**NOTE:** ArchiCOW requires Python 3.1 or later. Tested with Python 3.2 and 3.4.


Locations
---------

`ArchiCOW packages`_ are available from Cheese shop (PyPI).

`Documentation`_ is available online.

The `project page`_ is hosted on Bitbucket.

If you've never contributed to a project on Bitbucket, there is
a `quick start guide`_.

If you find something wrong or know of a missing feature, please
`create an issue`_ on the project page. If you find that inconvenient or have some security concerns, you could also drop me a line at <devel@beli.sk>.

.. _ArchiCOW packages: https://pypi.python.org/pypi/archicow
.. _documentation:     http://pythonhosted.org/archicow/
.. _project page:      https://bitbucket.org/beli-sk/archicow
.. _quick start guide: https://confluence.atlassian.com/display/BITBUCKET/Fork+a+Repo%2C+Compare+Code%2C+and+Create+a+Pull+Request
.. _create an issue:   https://bitbucket.org/beli-sk/archicow/issues


License
-------

Copyright 2014-2015 Michal Belica <devel@beli.sk>

::

    ArchiCOW is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    ArchiCOW is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with ArchiCOW.  If not, see < http://www.gnu.org/licenses/ >.

A copy of the license can be found in the ``LICENSE`` file in the
distribution.