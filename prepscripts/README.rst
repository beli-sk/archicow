ArchiCOW prepare scripts
========================

Scripts installed at remote (backup source) locations which prepare sources
for the backup, for example creating snapshots, database dumps, etc.

